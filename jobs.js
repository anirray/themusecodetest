"use strict";
/*
The Muse has a public API at:
https://www.themuse.com/developers/api/v2
Please write a script that does the following:
1. Downloads publicly available data from the “Jobs” endpoint.
2. Takes in an optional pages parameter that specifies how many pages of results to
download from the endpoint.
3. Saves this data set to a relational database, with a schema design of your choice.
After using this script to populate a database with all data (i.e., all pages), please write a query
to answer the following question:
How many jobs with the location "New York City Metro Area" were published from
September 1st to 30th 2016?
We commonly use Python for tasks like these, but you may use a language of our choice.
Please use a version control system like git and include a sufficient amount of usage
instructions so that we can quickly look at your script’s functionality.
We don’t want to take up too much of your time on this. If you’d like to code up a minimal
working version, then write a list of feature improvements & performance enhancements you’d
make to get this “production ready”, that’s totally fine.
Please freely reach out with any questions or concerns, and thank you!
*/

const endpoint = 'https://api-v2.themuse.com/jobs';
const apiKey = 'e1803eeb46f2a8a78553f64500b8c8726d9ddfa767138417e4a9d1bcea5a217f'; //ideally read in through environment variable
const sequelize = require('sequelize');
const request = require('request-promise-native');
const models = require('./models');

  models.sequelize.sync({ //sync models with MySQL DB, delete old entries
    force: true
  }).then(function() {
    console.log('db cleared, models synced');

    request({ //first get number of pages
        uri: endpoint,
        qs: {
          page: 1,
          api_key: apiKey
        },
        json: true
      })
      .then(res => {
        let pageCount = process.argv[2] ? process.argv[2] : res.page_count; //if there is an argument, use argument, if not use page count
        var promises = [];
        for (var i = 1; i < pageCount; i++) {
          promises.push(request({
            uri: endpoint,
            qs: {
              page: i, // -> uri + '?access_token=xxxxx%20xxxxx'
              api_key: apiKey
            },
            json: true // Automatically parses the JSON string in the response
          }));
        }
        return Promise.all(promises); //make request for each page. Cannot do all pages at once because page_limit is exceeded for total number of pages.
      })
      .then(pages => {
        const jobs = [];
        for(let i = 0; i < pages.length; i++){
          for(let j = 0; j < pages[i].results.length; j++){
            let job = pages[i].results[j];
            var locationName;
            if(job.locations[0]) locationName = job.locations[0]['name']; //check if name exists for location
            else locationName = null;
            const modifiedJob = {
              id: job.id,
              publication_date: Date.parse(job.publication_date),
              // name: job.name, //encoding error on one record
              location: locationName,
              company: job.company.name
            }

            jobs.push(modifiedJob);
          }
        }

        return models.Job.bulkCreate(jobs) //insert into Jobs table
          .then(() => {
            console.log('Jobs inserted into database');
          });
      })
      .then(() => {
        models.Job.findAll({ //find all jobs with requested query parameters
          where: {
            location: 'New York City Metro Area',
            publication_date: {
              $between: [Date.UTC(2016, 9, 1), Date.UTC(2016, 9, 30)]
            }
          }
        })
        .then(res => { //output results to console
          console.log("There are ", res.length, "jobs with the location New York City Metro Area that were published from September 1st to 30th 2016");
        })
      })
      .catch(err => { //output errors if any
        console.log(err);
      });
  });
