### Instructions

1. Ensure that you have Node v6.0.0 or greater, NPM v3.0.0 or greater, and MySQL v5.0 or greater installed.
2. Run `npm install` from project directory
3. In terminal type `mysql -u root` then type `CREATE DATABASE muse_jobs;` (ensure you have the mysql command set to your PATH via `set PATH $PATH /usr/local/mysql/bin/` if not already)
4. Set MySQL root user password to 'themuse' (or create your own user/password and update `./config/config.json` file);
5. In terminal, run `node jobs.js`, pass in the number of pages as an optional argument.
6. Check console output for query results


### Future Improvements

1. Normalize tables to follow third normal form
2. Use raw SQL queries instead of ORM, use indexing to speed up query
3. Include Category table, Company table, Level table, Location table
