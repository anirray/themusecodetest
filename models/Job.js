"use strict";

module.exports = function(sequelize, DataTypes) {
  var Job = sequelize.define("Job", {
    id:  {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    publication_date: DataTypes.BIGINT,
    location: DataTypes.STRING,
    company: DataTypes.STRING
  });

  return Job;
};
