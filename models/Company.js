"use strict";

module.exports = function(sequelize, DataTypes) {
  var Company = sequelize.define("Company", {
    name: DataTypes.STRING,
    short_name: DataTypes.STRING
  });

  return Company;
};
