"use strict";

module.exports = function(sequelize, DataTypes) {
  var Level = sequelize.define("Level", {
    name: DataTypes.STRING,
    short_name: DataTypes.STRING
  });

  return Level;
};
